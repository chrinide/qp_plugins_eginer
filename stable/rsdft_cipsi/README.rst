=================================================
Range separated DFT with CIPSI-like wave function
=================================================


This module allows to perform |RSDFT| calculations based on CIPSI wave functions. 
The available |RSDFT| functionals are |SRLDA| and |SRPBE|. 

The easy way to do it is use the script |qp_cipsi_rsh| (run qp_cipsi_rsh --help for detailed information). 

Such a script is also a good example to see how one can design a series of executions of |QP| programs can be done. 

